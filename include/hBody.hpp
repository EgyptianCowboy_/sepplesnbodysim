#ifndef HBODY
#define HBODY
#include <list>
#include <chrono>
#include <fmt/printf.h>
#include <memory>
#include <string>
#include "body.hpp"
#include "simHelper.hpp"

namespace simElem{
    template<typename T>
    class hBody : public body<T> {
    public:
	////////////////////////
	// Constructor	      //
	////////////////////////
	hBody(T xPos, T yPos, const T mass, const T diameter, const std::string name)
	    : body<T>(xPos, yPos, mass, diameter, name) {}
	hBody(T xPos, T yPos, const T mass, const T diameter, T xvel, T yvel, const std::string name)
	    : body<T>(xPos, yPos, mass, diameter, xvel, yvel, name) {}

	///////////////
	// Functions //
	///////////////
	constexpr auto calcBodyInf(simElem::body<T>& oppBody) noexcept -> void {
	    oppBody.addForceX(this->getMass(),this->getXPos()-oppBody.getXPos(),oppBody.distFromPoint(this->getXPos(),this->getYPos()));
	    oppBody.addForceY(this->getMass(),this->getYPos()-oppBody.getYPos(),oppBody.distFromPoint(this->getXPos(),this->getYPos()));
	}

	constexpr auto newPos(std::chrono::duration<T> elTime) noexcept -> void {
	    this->addVelX(this->getForceX() / this->getMass() * elTime.count());
	    this->addVelY(this->getForceY() / this->getMass() * elTime.count());
	    this->setXPos(this->getXPos() + this->getXVel() * elTime.count());
	    this->setYPos(this->getYPos() + this->getYVel() * elTime.count());
	    this->setForceX(0);
	    this->setForceY(0);
	}

	[[nodiscard]] constexpr auto boundCalc() noexcept -> bool {
	    return static_cast<bool>(((this->getXPos() > simHelper::BOARDWIDTH<T> || this->getXPos() < 0) || (this->getYPos() > simHelper::BOARDHEIGHT<T> || this->getYPos() < 0)));
	}
    private:
    };
}
#endif
