#ifndef SIM
#define SIM

namespace simHelper {
    template <typename T>
    constexpr static T BOARDWIDTH{800};

    template <typename T>
    constexpr static T BOARDHEIGHT{600};

    template <typename T>
    constexpr static T SCALE{1};

    template <typename T>
    constexpr static T GRAVCONST{6.674E-11};
}
#endif
