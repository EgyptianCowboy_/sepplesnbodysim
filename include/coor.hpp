#ifndef COOR
#define COOR

#include<math.h>

namespace simElem {
    template<typename T>
    class coor {
    public:
	coor(const T x, const T y) : pos{x, y} {}
	virtual ~coor() {}

	///////////////
	// Functions //
	///////////////
	constexpr auto distFromPoint(const T XPosO, const T YPosO) const noexcept -> T{
	    return std::sqrt((std::pow((XPosO-getXPos()),2)+std::pow((YPosO-getYPos()),2)));
	}

	/////////////
	// Setters //
	/////////////
	constexpr auto setXPos(const T x) noexcept -> void{
	    pos.XPos=x;
	}
	constexpr auto setYPos(const T x) noexcept -> void{
	    pos.YPos=x;
	}

	/////////////
	// Getters //
	/////////////
	[[nodiscard]] constexpr auto getXPos() const noexcept -> T{
	    return pos.XPos;
	}
	[[nodiscard]] constexpr auto getYPos() const noexcept -> T{
	    return pos.YPos;}

    private:
	struct pos_t {
	    T XPos;
	    T YPos;
	} pos;
    };
}
#endif
