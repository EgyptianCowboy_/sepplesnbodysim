#ifndef BOARD
#define BOARD
#include "coor.hpp"

namespace simElem {
    template<typename T>
    class board : public coor<double>{
    public:
	board(T width, T height, T x, T y)
	    : coor(x, y), bDim{width, height}, zero{width/2, height/2} {
	    }
	//~board();

	////////////////////
	// Setters	  //
	////////////////////

	constexpr auto setWidth(const T x) const noexcept -> void{
	    bDim.width=x;}
	constexpr auto setHeight(const T x) const noexcept -> void{
	    bDim.height=x;}
	constexpr auto setZeroX(const T x) const noexcept -> void{
	    zero.x=x;}
	constexpr auto setZeroY(const T y) const noexcept -> void {
	    zero.y=y;}

	////////////////////
	// Getters	  //
	////////////////////

	constexpr auto getWidth() const noexcept -> T{
	    return bDim.width;}
	constexpr auto getHeight() const noexcept -> T{
	    return bDim.height;}
	constexpr auto getZeroX() const noexcept -> T{
	    return zero.x;}
	constexpr auto getZeroY() const noexcept -> T{
	    return zero.y;}
    private:
	struct bDim_t {
	    const T width;
	    const T height;
	} bDim;
	struct zero_t {
	    const T x;
	    const T y;
	} zero;
    };
}
#endif
