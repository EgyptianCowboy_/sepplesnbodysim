#ifndef BFAC
#define BFAC

#include <fmt/format.h>
#include <list>
#include <array>
#include <memory>
#include <string>
#include "hBody.hpp"
#include "lBody.hpp"
#include "body.hpp"

namespace sim {
    template<
	typename T,
	typename TimeUnit,
	typename Clock
	>
    class sim;
}


namespace bodyFactory {
    template<typename T>
    class bodyFactory {
    public:
	[[nodiscard]] constexpr auto instance() const noexcept -> std::unique_ptr<bodyFactory<T>> {
	    if(_instance == nullptr)
		_instance = std::make_unique<bodyFactory<T>>();
	    return _instance;
	}
    private:
	bodyFactory() {}
	~bodyFactory() {}

	auto createBody(const T x, const T y, const T mass, const T diameter, const std::string name, const T xvel = 0, const T yvel = 0) noexcept ->std::unique_ptr<simElem::body<T>> {
	    std::unique_ptr<simElem::body<T>> tmpPtr = nullptr;

	    if (mass < 100 && mass > 0 && diameter > 0) {
		fmt::print("Creating body with low mass\n");
		if (!(tmpPtr = std::make_unique<simElem::lBody<T>>(x, y, mass, diameter, xvel, yvel, name))) {
		    fmt::print("Cannot create body\n");
		    std::terminate();
		}
		return tmpPtr;
	    } else if (mass > 100 && diameter > 0) {
		fmt::print("Creating body with high mass\n");
		if (!(tmpPtr = std::make_unique<simElem::hBody<T>>(x, y, mass, diameter, xvel, yvel, name))) {
		    fmt::print("Cannot create body\n");
		    std::terminate();
		}
		return tmpPtr;
	    } else {
		fmt::print("Cannot create body\n");
		return nullptr;
	    }
	}
	friend class sim::sim<T,std::chrono::seconds,std::chrono::system_clock>;
	const static std::unique_ptr<bodyFactory<T>> _instance;
    };
}
#endif
